const user = require('../models/users');

function getUser(req, res) {
    user.find({}).sort({ fecha: -1 }).exec(function(err, posts) {
        console.log('GET /api/posts');
        res.status(200).json(posts);
    });
}


function deleteUser(req, res) {
    user.findOneAndDelete({ usuario: req.body.usuario }, function(err, deleteUser) {
        if (err) res.json(err);
        console.log("api/posts POST");
        console.log("se elimino un post");
        console.log(deleteUser)
        res.json(deleteUser)
    });
}

function postUser(req, res) {
    const fromData = req.body;

    if (fromData.usuario == "") res.json({ message: "Inserte usuario" });
    if (fromData.password == "") res.json({ message: "Inserte password" });
    if (fromData.mail == "") res.json({ message: "Inserte una mail" });

    if ((fromData.titulo !== "") && (fromData.subtitulo !== "") && (fromData.descripcion !== "") && (fromData.contenido !== "")) {


        user.create({

            usuario: fromData.usuario,
            password: fromData.password,
            mail: fromData.mail,
            fecha: new Date()

        }, function(err, user) {
            if (err) res.json(err);


            console.log("se creo un post");
            res.json(user);

        });
    }
}

function updateUser(req, res) {
    const fromData = req.body;
    user.findOneAndUpdate({ usuario: fromData.usuario }, {
            $set: {
                usuario: fromData.usuario,
                password: fromData.password,
                mail: fromData.mail,
                fecha: new Date()
            }
        },
        function(err, updateUser) {
            if (err) res.json(err);

            console.log("/api/posts PUT");
            console.log("actualizado!");
            res.json(updateUser)
        });
}


module.exports = { getUser, postUser, deleteUser, updateUser }

//function postUser(req, res) {
//    const fromData = req.body;

//    user.insertMany({
//            usuario: fromData.usuario,
//            password: fromData.password,
//            mail: fromData.mail
//        },
//        function(err, user) {
//            if (err) res.json(err);
//
//
//            console.log("se creo un post");
//            res.json(user);
//
//        })
//}