const express = require('express');
const router = express.Router();
const user = require('../models/users');


// Se muestra la plantilla index.pug
router.get('/', (req, res) => {
    user.find({}, (err, users) => {
        console.log(users);

        if (err) throw err
            //console.log(users);
        res.render('index', { tittle: 'Inicio', users: users })
    })
})

//metodo post
router.post('/post', (req, res) => {
    const fromData = req.body;
    user.create({

        usuario: fromData.usuario,
        password: fromData.password,
        mail: fromData.mail,
        fecha: new Date()

    }, function(err, user) {
        if (err) res.json(err);


        console.log("se creo un post");
        //res.json(user);
        res.redirect('/')

    });
})

//metodo borrar, recibe un usuario
router.post('/delete', (req, res) => {
    const fromData = req.body;
    //console.log(fromData)
    console.log("recibido")
    user.findOneAndDelete({ usuario: fromData.usuario }, function(err, users) {
        if (err) res.json(err);
        console.log("api/posts POST");
        console.log("se elimino un post");
        console.log(users)
            //res.json(users)
    }, res.redirect('/'));
})

//metodo actualizar
router.post('/update', (req, res) => {
    const fromData = req.body;
    user.findOneAndUpdate({ usuario: fromData.usuario }, {
            $set: {
                usuario: fromData.usuario,
                password: fromData.password,
                mail: fromData.mail,
                fecha: new Date()
            }
        },
        function(err, users) {
            if (err) res.json(err);

            console.log("/api/posts PUT");
            console.log("actualizado!");
            //res.json(users)
        }, res.redirect('/'));
})

//vista admin.pug
router.get('/admin', (req, res) => {
    res.render('admin', { tittle: 'Administrador' });
});

//formulario Singup
router.get('/singup', (req, res) => {
    res.render('singup', { tittle: 'Registro' })
})

module.exports = router;