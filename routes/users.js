var express = require('express');
var router = express.Router();
var user = require('../controllers/users');

/* GET users listing. */
//router.get('/', function(req, res, next) {
//    res.send('respond with a resource');
//})


router.get('/', user.getUser);

router.put('/', user.updateUser);

router.delete('/', user.deleteUser);

router.post('/', user.postUser);


module.exports = router;