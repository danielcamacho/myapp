const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const users = new Schema({
    usuario: String,
    password: String,
    mail: String,
    fecha: {
        type: Date,
        default: Date.now()
    }
}, {
    versionKey: false // set to false then it wont create in mongodb
})

module.exports = mongoose.model('users', users);